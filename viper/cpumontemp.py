#!/usr/bin/python2.7
# coding: utf-8

import matplotlib.pyplot as plt
import re 
#import datetime

plotpoint=0
x=[]
y1=[]
y2=[]

with open('../logs/9-24-2018.log', 'w') as line:
        tl = re.findall('\d+', line)
        print(tl)

with open('../logs/9-24-2018.log','r') as f:
	for line in f:		
		tl = re.findall('\d+', line)
		#x.append(tl[4]+":"+tl[5]+":"+tl[6])
		x.append(plotpoint)
		y1.append(tl[0]+"."+tl[1])
		y2.append(tl[2]+"."+tl[3])
		#print tempentry
		if plotpoint > 24:
			break
		plotpoint=plotpoint+1


plt.subplot(2,1,1)
plt.plot(x,y1,'o-')
plt.title('CPU STATS')
plt.ylabel('CPU Usage (%)')
plt.xlabel("Time (Hour)")
plt.subplot(2,1,2)
plt.plot(x,y2,'o-')
plt.ylabel('Core Temp (C)')
plt.show()
plt.xlabel("Time (Hour)")

# DATA=$(date +"%Y-%m-%d")
# cd users/kathyly/desktop/logs/$DATA.log

# #!/usr/bin/env python3
# import os
# import re
# import yaml
# import logging

# from pprint import pprint
# from subprocess import PIPE, Popen

# import pytemp_logging

# path = os.path.abspath(os.path.dirname(__file__))

# def config():
#     """
#         Read the yaml config file.
#     """
#     try:
#         logging.debug('Opening config file.')
#         with open('%s/confgyml/config.yml' % path, 'r') as ymlfile:
#             cfg = yaml.load(ymlfile)
#             return cfg
        
#     except Exception as e:
#         logging.error('config failed: %s' % e)

# def cpu_temp():
#     """
#         Get CPU core temperature.
#     """
#     try:
#         cfg = config()
#         rx = cfg['regex']
#         df = Popen(['sensors'], stdout=PIPE, stdin=PIPE)
#         (out, err) = df.communicate()

#         rstring = rx['string']
#         data = re.findall(rstring, out.decode('UTF-8'))

#         temp = { 'cores':[] }

#         for r in data:
#             t = re.findall(rx['temp'], r)
#             out = {
#                 'id': re.search(rx['id'], r).group(0),
#                 'temp': t[0],
#                 'high': t[1],
#                 'crit': t[2]
#             }
#             temp['cores'].append(out)

#         return temp
        
#     except Exception as e:
#         logging.error('cpu_temp failed: %s' % e)

# def hdd_temp():
#     """
#         Get HDD temperature.
#     """
#     try:
#         cfg = config()
#         df = Popen(['hddtemp'] + cfg['drives'], stdout=PIPE, stdin=PIPE)
#         (out, err) = df.communicate()

#         string = create_string(out)

#         logging.debug('hdd_temp data: \n\n%s' % string)

#         return create_hdd_data(string)

#     except Exception as e:
#         logging.error('hdd_temp failed: %s' % e)

# def create_hdd_data(data):
#     """
#         Parse the hdd data.
#     """
#     try:
#         new_data = []
#         for r in data.split('\n'):
#             if r:
#                 new_data.append(row(r))

#         return new_data 

#     except Exception as e:
#         logging.error('create_hdd_data failed: %s' % e)

# def create_string(out):
#     """
#         Parse string.
#     """
#     try:
#         string = ''
#         for l in list(out):
#             if len(string):
#                 if not string[-1] == " " and chr(l) != " " and l != 194: 
#                     string += str(chr(l))
#             else:
#                 string += str(chr(l))

#         return string

#     except Exception as e:
#         logging.error('create_string failed: %s' % e)

# def row(r):
#     """
#     """

#     try:
#         r = r.split(':')
#         return {
#             'path': r[0],
#             'name': parse_name(r[1]),
#             'temp': r[2]
#         }
        
#     except Exception as e:
#         logging.error('row failed! Details: %s' % e)
#         return None

# def parse_name(string):
#     """
#         Remove unwanted characters from the name string.
#         TODO: move this to so it parses the whole string not just a small part like it does now.
#     """

#     logging.debug('Parsing name.')
#     try:
#         return re.search(r'[0-9A-Za-z]+', string).group(0)

#     except Exception as e:
#         logging.error('parse_name failed: %s' % e)

# def pytemp():
#     """
#         Create the final output.
#     """
#     try:
#         return {
#             'cpu': cpu_temp(),
#             'drives': hdd_temp()
#         }

#     except Exception as e:
#         logging.error('pytemp failed: %s' % e)


# if __name__ == "__main__":
#     temp = pytemp()
#     pprint(temp)
