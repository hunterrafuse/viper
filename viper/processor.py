from __future__ import print_function
import psutil
import platform
import os
import sys
import time
import subprocess
import re
import logging
from viper.macos_temp import MacOSXCpuTemp
from collections import Callable
from datetime import datetime, timedelta
from viper.monitoring import AbcMonitor
from viper.collectionz import LimitedTimeTable
from viper.temperature_reader import LinuxCpuTemperatureReader
from viper.temperature_reader import WindowsCpuTemperatureReader

# uncomment for file logging of CPU

##logger = logging.getLogger()
# logging.basicConfig(level=logging.DEBUG,
# filename='logs/processor.log')


class Cpu(AbcMonitor):
    """Monitoring system of the Central Processing Unit.

        :param monitoring_latency: time interval (in seconds) between calls of
            the CPU scanner.
        :type monitoring_latency: int, float
        :param stats_interval: time interval (in seconds) between calls of the
            statistics collector.
        :type stats_interval: int, float

        Usage example:

        .. code-block:: python

            >>> from pyspectator.processor import Cpu
            >>> from time import sleep
            >>> cpu = Cpu(monitoring_latency=1)
            >>> cpu.name
            'Intel(R) Core(TM)2 Duo CPU     T6570  @ 2.10GHz'
            >>> cpu.count
            2
            >>> with cpu:
            ...     for _ in range(8):
            ...        cpu.load, cpu.temperature
            ...        sleep(1.1)
            ...
            (22.6, 55)
            (6.1, 55)
            (5.5, 54)
            (7.1, 54)
            (5.6, 54)
            (7.0, 54)
            (10.2, 54)
            (6.6, 54)

    """

    # region initialization

    def __init__(self, monitoring_latency, stats_interval=None):
        super().__init__(monitoring_latency)
        self.__name = Cpu.__get_processor_name()
        self.__count = psutil.cpu_count()
        self.__load = None
        self.__battery = None
        self.__macostemp = self.__get_macostemp()
        # Init temperature reader
        
        # Prepare to collect statistics
        if stats_interval is None:
            stats_interval = timedelta(hours=1)
        self.__load_stats = LimitedTimeTable(stats_interval)
        self.__temperature_stats = LimitedTimeTable(stats_interval)
        # Read updating value at first time
        self._monitoring_action()

    # endregion

    # region properties
    @property
    def battery(self):

        return self.__battery

    @property
    def name(self):
        """Full name of the CPU.

        :getter: Return full name of the CPU. Return ``None`` if undetermined.
        :setter: Not available.
        :type: string, None
        """
        return self.__name

    @property
    def count(self):
        """Amount of a CPU cores.

        :getter: Return the number of logical CPUs in the system. Return
            ``None`` if undetermined.
        :setter: Not available.
        :type: int, None
        """
        return self.__count

    @property
    def load(self):
        """CPU load in percent.

        :getter: Return CPU load in percent. From 0.00 to 100.00 or ``None``
            if undetermined.
        :setter: Not available.
        :type: float, None
        """
        psutil.cpu_stats()

        return self.__load

    @property
    def temperature(self):
        """Temperature (in Celsius) of the CPU.

        :getter: Return temperature (in Celsius) of the CPU. Return ``None``
            if undetermined.
        :setter: Not available.
        :type: int, None
        """

        return self.__temperature
    @property
    def macostemp(self):
        return self.__macostemp
    @property
    def load_stats(self):
        """Statistics about CPU load.

        :getter: Return statistics about CPU load.
        :setter: Not available.
        :type: pyspectator.collection.LimitedTimeTable
        """
        return self.__load_stats

    @property
    def temperature_stats(self):
        """Statistics about CPU temperature.

        :getter: Return statistics about CPU temperature.
        :setter: Not available.
        :type: pyspectator.collection.LimitedTimeTable
        """
        return self.__temperature_stats

    # endregion

    # region methods

    def _monitoring_action(self):
        now = datetime.now()
        self.__load = psutil.cpu_percent()
        self.__load_stats[now] = self.__load
        # if isinstance(self.__temperature_reader, Callable):
        #     self.__temperature = self.__temperature_reader()
        #     self.__temperature_stats[now] = self.__temperature

    # @classmethod
    # def __get_fan_speed(self):
    #     if not hasattr(psutil, sensors_fans()):
    #         return sys.exit("platform not supported")
    # fans = psutil.sensors_fans()
    # if not fans:
    #     print("no fans detected")
    # for name, entries in fans.items():
    #     print(name)
    #     for entry in entries:
    #         print("    %-20s %s RPM" % (entry.label or name, entry.current))
    #     print()

    @classmethod
    def __get_processor_name(cls):
        cpu_name = None
        os_name = platform.system()
        if os_name == 'Windows':
            cpu_name = platform.processor()
        elif os_name == 'Darwin':
            os.environ['PATH'] = os.environ['PATH'] + os.pathsep + '/usr/sbin'
            command = ('sysctl', '-n', 'machdep.cpu.brand_string')
            output = subprocess.check_output(command)
            if output:
                cpu_name = output.decode().strip()
        elif os_name == 'Linux':
            all_info = subprocess.check_output('cat /proc/cpuinfo', shell=True)
            all_info = all_info.strip().split(os.linesep.encode())
            for line in all_info:
                line = line.decode()
                if 'model name' not in line:
                    continue
                cpu_name = re.sub('.*model name.*:', str(), line, 1).strip()
                break
        return cpu_name
    @classmethod
    def __get_macostemp(self):
        FILENAME = 'viper/logs/base.txt'
        
        call = MacOSXCpuTemp.getTemp(self)
        # MacOSXCpuTemp.getTemp(self)
        
        while True:
            l = open(FILENAME, 'r')        
            line = l.readline()
                # for line in l:
                #     line = l.readline()
            for i in range(2):
                return line
        # continue
        # return call
        # exec(call)
        # time.sleep(10)
        # return call

    @classmethod
    def battery_stats(self):
        batterystats = None

        def secshours(secs):
            mm, ss = divmod(secs, 60)
            hh, mm = divmod(mm, 60)
            return "%d:%02d:%02d" % (hh, mm, ss)

        def getBattery(self):
            if not hasattr(psutil, "sensors_battery"):
                return sys.exit("platform not supported")
            batt = psutil.sensors_battery()
            if batt is None:
                return sys.exit("no battery is installed")

            print("charge:     %s%%" % round(batt.percent, 2))
            if batt.power_plugged:
                print("status:     %s" % (
                    "charging" if batt.percent < 100 else "fully charged"))
                print("plugged in: yes")
            else:
                print("left:       %s" % secshours(batt.secsleft))
                print("status:     %s" % "discharging")
                print("plugged in: no")
            return batterystats 

    # endregion

    pass


__all__ = ['Cpu']
