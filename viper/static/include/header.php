<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
	<title>Fiesta</title>
	<meta name="description" content="Description of the page less than 150 characters">
	<link href="css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700|Source+Sans+Pro" rel="stylesheet"> 
	 <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,700" rel="stylesheet"> 

	<!-- OG tags -->
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://example.com/page.html">
	<meta property="og:title" content="Content Title">
	<meta property="og:image" content="https://example.com/image.jpg">
	<meta property="og:description" content="Description Here">
	<meta property="og:site_name" content="Site Name">
	<meta property="og:locale" content="en_US">
	<meta property="og:image:width" content="1200">
	<meta property="og:image:height" content="630">

	<!-- Twitter card -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@site_account">
	<meta name="twitter:creator" content="@individual_account">
	<meta name="twitter:url" content="https://example.com/page.html">
	<meta name="twitter:title" content="Content Title">
	<meta name="twitter:description" content="Content description less than 200 characters">
	<meta name="twitter:image" content="https://example.com/image.jpg">

	<!-- Favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
	<link rel="manifest" href="images/favicon/site.webmanifest">
	<link rel="mask-icon" href="images/favicon/safari-pinned-tab.svg" color="#800080">
	<meta name="msapplication-TileColor" content="#800080">
	<meta name="theme-color" content="#ffffff">
	
	<!--[if lt IE 9]>
	<script src="js/vendor/html5shiv.min.js"></script>
	<script src="js/vendor/selectivizr.js"></script>
	<script src="js/vendor/respond.min.js"></script>
	<![endif]-->

	<script>
		var app={queue:[],ready:function(a){this.isReady?a():this.queue.push(a)}};
	</script>
</head>

<body>

	<header class="c-header" id="site-header">
		<a href="" class="logo"><img src="images/logo.svg" alt=""></a>
		<div class="c-nav-close" id="nav-close"></div>
		<nav class="c-main-nav" id="main-nav">
			<ul>
				<li class="m-active"><a href="">Home</a></li>
				<li class="m-parent"><a href="">Products</a>
					<ul>
						<li><a href="">Sub item 1</a></li>
						<li><a href="">Sub item 2</a></li>
					</ul>
				</li>
				<li class="m-parent">
					<a href="">Services</a>
					<ul>
						<li><a href="">Sub item 1</a></li>
						<li><a href="">Sub item 2</a></li>
						<li><a href="">Sub item 3</a></li>
						<li><a href="">Sub item 4</a></li>
					</ul>
				</li>
				<li><a href="">About us</a></li>
				<li><a href="">Contact</a></li>
			</ul>
		</nav>
		<button class="c-nav-toggle" id="nav-toggle">
			<span class="lines">
				<span class="line"></span>
				<span class="line"></span>
				<span class="line"></span>
			</span>
		</button>
	</header>
	