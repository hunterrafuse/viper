<section class="c-hero">
	<picture>
		<source srcset="images/hero-image.jpg" media="(min-width: 0px)">
		<img src="images/hero-image.jpg" alt="">
	</picture>
	<div class="inner">
		<h1>Fiesta<!-- <span>boilerplate</span>--></h1>
		<a href="download/fiesta.zip" class="c-button">Get it!</a>
		<p><a class="docs-link" href="docs">Utilities</a></p>
	</div>
	<?php require_once('include/share.php'); ?>
</section>