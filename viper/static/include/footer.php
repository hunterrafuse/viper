	<footer class="c-footer">
		<div class="social">
			<a href="" class="facebook" target="_blank" rel="noopener"></a>
			<a href="" class="twitter" target="_blank" rel="noopener"></a>
			<a href="" class="instagram" target="_blank" rel="noopener"></a>
			<a href="" class="youtube" target="_blank" rel="noopener"></a>
		</div>
	</footer>

	<!-- build:js /resources/js/script.js -->
	<script src="js/polyfills.js"></script>
	<script src="js/vendor/isMobile.min.js"></script>
	<script src="js/vendor/delfin.js"></script>
	<script src="js/App.js"></script>
	<script src="js/Header.js"></script>
	<script src="js/Navigation.js"></script>
	<script src="js/vendor/cookieconsent.js"></script>

	<!-- Utils. Remove ones not needed -->
	<script src="js/utils/Utils.js"></script>
	<script src="js/utils/Accordion.js"></script>
	<script src="js/utils/Carousel.js"></script>
	<script src="js/utils/CookieManager.js"></script>
	<script src="js/utils/Fader.js"></script>
	<script src="js/utils/Filter.js"></script>
	<script src="js/utils/Form.js"></script>
	<script src="js/utils/GoogleMap.js"></script>
	<script src="js/utils/Overlay.js"></script>
	<script src="js/utils/ResponsiveImage.js"></script>
	<script src="js/utils/Scroller.js"></script>
	<script src="js/utils/TabView.js"></script>
	<script src="js/utils/Toggler.js"></script>
	<script src="js/utils/Validator.js"></script>
	<script src="js/utils/YoutubeVideo.js"></script>
	<!-- endbuild -->

	<script>
		app.ready(function() {
			cookieConsent({
				text: "This website uses cookies. By continuing to browse it, you agree to their use. ",
				linkText: "Read more",
				linkUrl: "https://www.google.fi",
				buttonText: "OK"
			});
		});
	</script>