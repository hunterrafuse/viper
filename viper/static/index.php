<?php require_once('include/header.php'); ?>

	<main>
		<?php require_once('include/hero.php'); ?>
		<div class="container">
			<section class="section">
				<div class="c-breadcrumb">
					<a href=""><span>Wikia</span></a>
					<a href=""><span>Dishonored</span></a>
					<span>Dunwall</span>
				</div>
				<div class="g-article">
					<h1>Dunwall</h1>
					<p>Dunwall, the capital of Gristol and the Empire of the Isles, is an industrial whaling city situated on the Wrenhaven River. Its strategic importance has made it the target of numerous events throughout recorded history. </p>
					<h2>Geography</h2>
					<p>Dunwall is one of the largest cities in <a href="">the Empire</a>, spanning an area of 19.65 square miles, and sitting at an elevation of 125 feet.[2] It is divided in two by the Wrenhaven, a vast river which serves as the main thoroughfare for whaling trawlers and other commercial traffic. Kingsparrow Island, located where the river meets the sea, is the only island known to exist off the city. Dunwall is dominated by rocky outcroppings and numerous cliffs, which are topped by multiple factories and manors. </p>
					<p>Dunwall is an expansive city comprised of numerous districts and locations: industrial, commercial and residential. The districts north of the Wrenhaven are generally wealthier than those located on its southern counterpart. The Estate District, home of the aristocracy, and Dunwall Tower, the seat of the Empire, are located on the north side, dominated by the Clocktower of Dunwall, the tallest building in the city, save for Dunwall Tower itself.[3] Industrial districts such as Slaughterhouse Row and Drapers Ward are mostly located near the river, facilitating transport by trade vessels. Transport of goods and people is also assured by rails circulating all over the city. Lastly, Kaldwin's Bridge joins each side of the river while still allowing access to large vessels inland. </p>
					<h3>Watercourses</h3>
					<ul>
						<li>Wrenhaven River: The main river flowing through Dunwall. Used mainly by whaling trawlers to access the slaughterhouses alongside the river. </li>
						<li>Dunwall Sewers: The city's waterworks used to evacuate wastewater directly into the Wrenhaven. They cover most of Dunwall's underground. </li>
					</ul>
					<h3>Districts</h3>
					<ol>
						<li>Distillery District: A district named after the old Dunwall Whiskey Distillery, which is located there. </li>
						<li>Holger Square: Location of the Office of the High Overseer. It serves as headquarters for the Overseers of the Abbey of the Everyman in Dunwall. </li>
						<li>Estate District: A wealthy, upper class district housing the estates of Dunwall's most prestigious families, such as the Boyles and the Morays. </li>
					</ol>
				</div>
				<p><a href="" class="c-button">Read more</a></p>
			</section>
		</div>
		<!--<script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>
		<script>
			var jq = jQuery.noConflict();
		</script>
		<script src="//assets.juicer.io/embed-no-jquery.js" type="text/javascript"></script>
		<link href="https://assets.juicer.io/embed.css" media="all" rel="stylesheet" type="text/css" />
		<ul class="juicer-feed" data-feed-id="abbasillikset"></ul>-->
	</main>

<?php require_once('include/footer.php'); ?>
	
</body>
</html>