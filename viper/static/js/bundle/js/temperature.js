
var smc = require('smc');

var temperature = document.getElementById('temperature-stats');

function getTemp() {
   Object.keys(smc.metrics).forEach(function (key) {
      var value = smc.get(key);
      if (value > 0) {
         temperature(key, smc.metrics[key] + ':', value).innerHTML = value;
      }
   });

   var i,
       f = smc.fans();

   for (i = 0; i < f; i++) {
      console.log('F' + i + 'Ac', 'Fan', i, 'RPM:', smc.fanRpm(i));
   }
}