class App {

	constructor(queue) {
		this.isReady = false;
		this.readyQueue = queue;
		this.mobile = false;
	}

	init() {
		d.ready(() => {
		// document.addEventListener("DOMContentLoaded", () => {
			this.isReady = true;
			this.start();
			this.processQueue();
		});
	}

	ready(fn) {
		if(this.isReady) {
			fn();
		} else {
			this.readyQueue.push(fn);
		}
	}

	processQueue() {
		while(this.readyQueue.length) {
			this.readyQueue.shift()();
		}
	}

	start() {
		// Check for mobile
		if(isMobile.phone || isMobile.tablet) {
			this.mobile = true;
			d("html").classList.add("mobile-device");
		}

		// Header
		new Header();

		// Navigation
		new Navigation();
	}
}

const d = Delfin;

app = new App(app.queue);
app.init();