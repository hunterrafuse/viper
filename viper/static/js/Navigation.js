class Navigation {

	constructor() {
		this.parentIsLink = true;
		this.showActiveMenu = true;
		
		this.build();
	}

	build() {
		d("#nav-toggle").addEventListener("click", function(e) {
			d("html").classList.toggle("m-nav-open");
			d("#main-nav").scrollTo(0,0);
		});

		d("#nav-close").addEventListener("click", function(e) {
			d("html").classList.remove("m-nav-open");
			d("#main-nav").scrollTo(0,0);
		});

		this.toggleNavigation();
	}

	toggleNavigation() {
		$$(".m-parent", this.el).forEach((i) => {
			d("a", i).insertAdjacentHTML("beforebegin", '<span class="toggle"><span>+</span><span>-</span></span>');
			
			// Open active menu
			if(i.classList.contains("m-active") && this.showActiveMenu) {
				d("ul", i).classList.add("m-visible");
				d(".toggle", i).classList.add("m-active");
			}

			// Parent is link?
			if(this.parentIsLink) {
				d(".toggle", i).addEventListener("click", function(e) {
					e.preventDefault();
					toggle(i, this);
				});
			}
			else {
				i.addEventListener("click", function(e) {
					e.preventDefault();
					toggle(i, this);
				});
			}

			var toggle = function(item, button) {
				d("ul", item).classList.toggle("m-visible");
				button.classList.toggle("m-active");

				// Close children
				$$(".m-parent", item).forEach(function(j) {
					d("ul", j).classList.remove("m-visible");
					d("toggle", j).classList.remove("m-active");
				});
			};
		});
	}
}