class Scroller extends Utils {

	constructor(el) {
		super(el);

		this.target = el.querySelector(".inner");
		this.mouseTarget = el.querySelector(".inner > div");
		this.internalScrollLeft = 0;
		this.pressed = false;

		this.build();
	}

	static setup(arr) {
		return super.setup(arr, Scroller, "[data-scroller]");
	}

	build() {
		this.scroll = this.el.querySelector(".inner");

		this.el.classList.add("m-left");

		this.scroll.addEventListener("scroll", () => {
			this.updateScrollPosition();
		});

		window.addEventListener("resize", e => {
			this.updateScrollPosition();
		});

		this.updateScrollPosition();

		// Dragging
		if(!app.mobile) {
			this.mouseTarget.addEventListener("mousedown", (e) => {
				this.mousedown(e);
				e.preventDefault();
				e.stopPropagation();
			});

			document.addEventListener("mousemove", (e) => {
				this.mousemove(e);
			});

			document.addEventListener("mouseup", (e) => {
				this.mouseup(e);
			});

			this.animate();

			this.target.addEventListener("mousedown", (e) => {
				this.active = false;
			});
		}
	}

	updateScrollPosition() {
		const width = this.el.offsetWidth;
		const scrollWidth = this.scroll.scrollWidth;

		if(this.scroll.scrollLeft == 0) {
			this.el.classList.add("m-left");
		}
		else {
			this.el.classList.remove("m-left");
		}
		
		if(this.scroll.scrollLeft >= scrollWidth - width) {
			this.el.classList.add("m-right");
		}
		else {
			if(scrollWidth - width > 0) {
				this.el.classList.remove("m-right");	
			}
		}
	}

	mousedown(e) {
		this.startX = e.clientX;
		this.xPos = 0;
		this.startScroll = this.target.scrollLeft;
		this.active = true;
		this.pressed = true;
	}

	mousemove(e) {
		if(this.pressed) {
			this.xPos = (e.clientX - this.startX);
		}
	}

	mouseup(e) {
		e.preventDefault();
		this.pressed = false;
	}

	animate() {
		requestAnimationFrame(this.animate.bind(this));
		
		if(this.active) {
			const targetLeft = (this.startScroll - this.xPos);
			const currentLeft = this.target.scrollLeft;
			const temp = (targetLeft - currentLeft) * 0.05;

			//if movement is small enough and mouse is no longer being pressed
			if (Math.abs(temp) < 0.1 && !this.pressed) {
			 	this.active = false;
			}
			this.target.scrollLeft += temp;
		}
	}
}