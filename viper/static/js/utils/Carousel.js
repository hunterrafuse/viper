class Carousel {

	constructor(el, options) {
		this.carousel = el;
		this.dots = d(".carousel-dots", this.carousel);
		this.nav = d(".carousel-nav", this.carousel);
		this.progress = d(".carousel-progress", this.carousel);
		this.options = {};

		for(let i in options) {
			if(options.hasOwnProperty(i)) {
				this.options[i] = options[i];
			}
		}

		// Runs when slide transition begins
		this.options.callback = () => {
			this.updateControl();

			if(options.callback) {
				options.callback();
			}
		}

		this.options.transitionEnd = () => {
			this.setProgressInterval();

			if(options.transitionEnd) {
				options.transitionEnd();
			}
		}

		this.swipe = Swipe(el.querySelector(".swipe"), this.options);

		const num = this.swipe.getNumSlides();

		// Exit when just 1 slide
		if(num == 1) {
			if(this.nav) {
				this.nav.style.display = "none";
			}

			if(this.dots) {
				this.dots.style.display = "none";
			}

			return false;
		}

		// Dots
		if(this.dots) {
			for(let i = 0; i < num; i++) {
				const item = document.createElement("button");
				item.addEventListener("click", this.onClickControl(i));

				this.dots.appendChild(item);
			}
		}

		// Nav
		const next = this.carousel.querySelector(".carousel-next");
		const prev = this.carousel.querySelector(".carousel-prev");

		if(next) {
			next.addEventListener("click", e => {
				this.swipe.next();
			});
		}

		if(prev) {
			prev.addEventListener("click", e => {
				this.swipe.prev();
			});
		}

		if(this.options.stopOnMouseEnter && !this.progress) {
			// Stop on mouseenter
			this.carousel.addEventListener("mouseenter", e => {
				this.swipe.stop();
				// this.clearProgressInterval(true);
			});

			// Restart on mouseleave
			this.carousel.addEventListener("mouseleave", e => {
				this.swipe.restart();
				// this.setProgressInterval();
			});	
		}

		this.update(1, true);
	}

	updateControl() {
		this.update(this.swipe.getPos() + 1);
	}

	update(num, first) {
		// Video
		$$("video", this.carousel).forEach(i => {
			i.pause();
			i.currentTime = 0;
		});

		const el = this.carousel.querySelector(".swipe-wrap").children[num - 1];
		const video = el.querySelector("video");

		if(video) {
			video.play();
		}
		
		// Set active dot
		$$(".carousel-dots > button", this.carousel).forEach(i => {
			i.className = "";
		});

		if(this.dots) {
			d(".carousel-dots > button:nth-child(" + (num) + ")", this.carousel).classList.add("m-active");
		}

		// Clear interval
		this.clearProgressInterval();

		// Set interval for first slide
		if(first) {
			this.setProgressInterval();
		}
	}

	clearProgressInterval(reset) {
		if(this.progress && this.interval) {
			clearInterval(this.interval);
			this.progress.style.width = "100%";
		}
	}

	setProgressInterval() {
		if(this.progress) {
			let time = 0;
			const interval = 20;
			const refDate = new Date().getTime();
			
			this.interval = setInterval(() => {
				const date = new Date().getTime();
				const elapsed = date - refDate;
				let pct = Math.min(elapsed / (this.options.auto), 1);

				// Update progress
				this.progress.style.width = (pct * this.carousel.offsetWidth) + "px";
			}, interval);	
		}
	}

	getSwipe() {
		return this.swipe;
	}

	onClickControl(i) {
		return e => {
			this.update(i + 1);
			this.swipe.slide(i);
		}
	}
}