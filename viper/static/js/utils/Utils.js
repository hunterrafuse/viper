class Utils {

	constructor(el) {
		this.el = el;
	}

	static setup(arr, ref, selector) {
		arr = arr || $$(selector);

		if(arr.length == 0) {
			return false;
		}

		const items = [];

		arr.forEach(i => {
			items.push(new ref(i));
		});

		return items;
	}

	addEventListener(name, fn) {
		this.el.addEventListener(name, e => {
			fn.call(this, e);
		});
	}

	dispatchEvent(name, props) {
		this.el.dispatchEvent(new CustomEvent(name, props));
	}
}