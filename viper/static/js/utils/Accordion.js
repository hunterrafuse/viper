class Accordion extends Utils {

	constructor(el) {
		super(el);

		this.settings = {
			hash: false
		}

		this.build();
	}

	static setup(arr) {
		return super.setup(arr, Accordion, "[data-accordion]");
	}

	build() {
		// Scroll to anchor
		let hash = window.location.hash.substring(1, window.location.hash.length);

		if(this.settings.hash && hash && hash != "!") {
			const el = this.el.querySelector("li[data-hash='" + hash + "']");

			if(el) {
				el.classList.add("m-active");

				const pos = el.getBoundingClientRect().top + d.scrollTop();
				window.scroll({top: pos, behavior: "smooth"});
			}
		}

		// Setup click event
		$$(".item", this.el).forEach(i => {
			i.querySelector(".title button").addEventListener("click", e => {
				const parent = i.closest("li");
				parent.classList.toggle("m-active");
				// Toggler.toggle(i.parentNode.querySelector(".content"));

				// Set ARIA
				e.target.setAttribute("aria-expanded", i.classList.contains("m-active"));

				// Close others
				if(this.settings.hash) {
					$$("li", this.el).forEach(j => {
						if(i != j) {
							j.classList.remove("m-active");
						}
					});	
				}

				// Scroll to
				if(this.settings.hash && i.classList.contains("m-active")) {
					const pos = i.getBoundingClientRect().top + d.scrollTop();
					window.scroll({top: pos, behavior: "smooth"});
				}

				hash = i.getAttribute("data-hash");

				if(this.settings.hash && hash) {
					// Hash
					if(i.classList.contains("m-active")) {
						location.hash = hash;
					}
					else {
						location.hash = '#!';
					}	
				}
			});
		});	
	}
}