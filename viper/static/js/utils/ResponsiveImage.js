// class Images {

// 	constructor() {
// 		this.images = [];
		
// 		this.build(Delfin.$$("[data-srcset]"));
// 	}

// 	build(arr) {
// 		arr.forEach((i) => {
// 			this.images.push(new Image(i));
// 		});
// 	}

// 	load(list) {
// 		list.forEach(function(i) {
// 			i.setAttribute("data-load", "load");
// 		});
		
// 		for(var i = 0; i < this.images.length; i++) {
// 			this.images[i].update();
// 		}
// 	}
// }

class ResponsiveImage extends Utils {

	constructor(el) {
		super(el);

		this.settings = {
			devicePixelRatio: false
		}

		this.images = [];
		this.sizes = {};
		this.useDpr = (this.settings.devicePixelRatio && this.el.getAttribute("data-dpr") != "false") || this.el.getAttribute("data-dpr") == "true";
		this.currentSize = -1;
		this.currentMq = false;
		this.load = this.el.getAttribute("data-load");

		this.build();
	}

	static setup(arr) {
		return super.setup(arr, ResponsiveImage, "[data-srcset]");
	}

	build() {
		const srcset = this.el.getAttribute("data-srcset");
		const srcsetArr = srcset.split(",");

		for(let i = 0; i < srcsetArr.length; i++) {
			// Single image
			const img = srcsetArr[i].replace(/^\s+|\s+$/g, '');
			// Split url and size
			const imgArr = img.split(" ");
			const unitValue = imgArr[1] ? imgArr[1] : "0w";
			const unit = unitValue.replace(/[0-9.]/g, "");
			const size = unitValue.replace(/[a-zA-Z]/g, "");
			const obj = {id: i, url: imgArr[0], size: size, unit: unit};

			this.images[i] = obj;
		}

		// Sort array
		this.images.sort(function(a,b) {
			return parseFloat(a.size) - parseFloat(b.size);
		});

		// Sizes
		const sizesAttr = this.el.getAttribute("data-sizes");

		if(sizesAttr) {
			this.sizes = {unit: sizesAttr.replace(/[0-9.]/g, ""), size: sizesAttr.replace(/[a-zA-Z]/g, "")};
		}

		this.update();

		var obj = this;

		window.addEventListener("resize", e => {
			obj.update();
		});
	}

	update() {
		// Choose largest image as default
		let selectedSize = this.images.length - 1;
		const dpr = window.devicePixelRatio || 1;
		const dprValue = this.useDpr ? dpr : 1;
		const width = this.sizes === true ? this.sizes.size : window.innerWidth;

		for(let i = 0; i < this.images.length; i++) {
			const sizeW = this.images[i].size / (dprValue);
			const sizeX = this.images[i].size;
			const unit = this.images[i].unit;
			
			if(unit == "w") {
				if(sizeW / width >= 1) {
					selectedSize = i;
					break;
				}
			}

			if(unit == "x") {
				if(sizeX > dpr - 0.5) {
					selectedSize = i;
					break;
				}
			}
		}
		
		// Lazy load
		this.load = this.el.getAttribute("data-load");
		
		// Check if image should be loaded
		const mq = this.load == "load" || (this.load != undefined ? window.matchMedia("(min-width: " + Delfin.breakpoints[this.load] + ")").matches : true);

		if(mq != this.currentMq) {
			this.currentMq = mq;

			if(mq) {
				this.setImage(selectedSize);
				return false;
			}
		}

		// Switch image only if size has changed
		if(mq && selectedSize != this.currentSize) {
			this.setImage(selectedSize);
			this.currentSize = selectedSize;
		}
	}

	setImage(size) {
		if(this.el.tagName == "IMG") {
			this.el.setAttribute("src", this.images[size].url);
		}
		else {
			this.el.style.backgroundImage = "url(" + this.images[size].url + ")";
		}
	}

	loadImage() {
		this.el.setAttribute("data-load", "load");
		this.update();
	}
}