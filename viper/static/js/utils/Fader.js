class Fader {

	constructor() {
		this.settings = {
			repeat: false
		}

		this.assets = [];
		this.windowHeight = window.innerHeight;
		this.onScroll = this.onScroll.bind(this);
		this.onResize = this.onResize.bind(this);

		$$("[class*='fade-']").forEach(i => {
			const delay = i.getAttribute("data-delay");
			const scroll = i.getAttribute("data-scroll") ? true : false;
			const obj = {};
			obj.el = i;
			obj.scroll = scroll;
			obj.offset = i.getAttribute("data-offset") ? parseInt(i.getAttribute("data-offset"), 10) : 0;

			if(delay) {
				obj.el.style.transitionDelay = i.getAttribute("data-delay") + "s";
			}

			this.assets.push(obj);
		});

		this.update(false);

		window.addEventListener("scroll", this.onScroll);
		window.addEventListener("resize", this.onResize);
	}

	onScroll() {
		this.update(true);
	}

	onResize() {
		this.update(false);
	}

	update(scroll) {
		this.assets.forEach((i) => {
			if(i.scroll && !scroll) return;

			const el = i.el;
			const top = el.getBoundingClientRect().top;
			
			if(top + i.offset < this.windowHeight) {
				el.classList.add("fade-in");

				if(!this.settings.repeat) {
					const index = this.assets.indexOf(i);
					this.assets.splice(index, 1);
				}
			}
			else {
				if(this.settings.repeat) {
					el.classList.remove("fade-in");
				}
			}
		});

		if(this.assets.length == 0) {
			this.destroy();
		}
	}

	destroy() {
		window.removeEventListener("scroll", this.onScroll);
		window.removeEventListener("resize", this.onResize);
	}
}