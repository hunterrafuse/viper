class LazyLoader {

	constructor() {
		this.assets = [];

		window.addEventListener("scroll", (e) => {
			this.update();
		});

		window.addEventListener("resize", (e) => {
			this.update();
		});
	}

	update() {
		this.assets.forEach(i => {
			if(i.loaded) {
				return;
			}

			const top = i.element.el.getBoundingClientRect().top;
			
			if(top + i.offset < window.innerHeight) {
				i.element.loadImage();
				i.loaded = true;
			}
		});
	}

	load(el, options) {
		const options2 = options || {};
		const offset = options2.offset === undefined ? 0 : options2.offset;

		if(Array.isArray(el)) {
			el.forEach(i => {
				this.addAsset(i, offset);
			});
		}
		else {
			this.addAsset(el, offset);
		}

		this.update();
	}

	addAsset(el, offset) {
		const obj = {};
		obj.element = el;
		obj.loaded = false;
		obj.offset = offset;

		this.assets.push(obj);
	}
}