class Form extends Utils {

	constructor(el) {
		super(el);

		this.button = el.querySelector("[data-submit]");
		this.sending = false;

		this.build();
	}

	static setup(arr) {
		return super.setup(arr, Form, "[data-form]");
	}

	build() {
		// Clear errors on click
		$$("[data-required]", this.el).forEach(i => {
			i.addEventListener("change", e => {
				i.closest(".field").classList.remove("error");
			});
		});

		// Set send event
		this.el.querySelector("[data-submit]").addEventListener("click", e => {
			e.preventDefault();

			if(!this.sending) {
				this.send();
			}
		});	
	}

	send() {
		const options = {
			submitEvent: false,
			callback: (errors) => {
				// Loop through validation errors
				errors.forEach((i) => {
					// console.log(i, i.element.closest("label"));
					i.element.closest(".field").classList.add("error");
				});

				// If errors, scroll to first error
				if(errors.length > 0) {
					o.showMessage(errors[0].element);
				}
			}
		}

		// Validator
		const validator = new Validator(this.el, options);

		const o = this;

		if(validator.validate()) {
			const urltosend = this.el.getAttribute("data-action");

			fetch(urltosend, {
				method: "POST", // GET or POST
				body: JSON.stringify(this.serialize()),
			}).then(response => {
				return response.json();
			}).then(data => {
				this.success(data);
			}).catch(err => {
				this.error();
			});

			this.startLoader();
		}
	}

	serialize() {
		const elems = this.el.elements;
		let data = {};

		for(let i = 0; i < elems.length; i += 1) {
			const element = elems[i];
			const type = element.type;
			const name = element.name;

			switch (type) {
				case 'checkbox':
					data[name] = element.checked;
					break;
				case 'radio':
					if(element.checked) {
						data[name] = element.value;
					}
					break;
				default:
					data[name] = element.value;
					break;
			}
		}
		
		return data;
	}

	success(data) {
		// Hide errors
		if(data.success) {
			this.el.querySelector("[data-fetch-error]").style.display = "none";

			// Hide form and show success
			d("[data-form-content]").style.display = "none";
			this.el.reset();

			this.showMessage(d("[data-success]"), true);	
		}
		// Back end validation errors
		else {
			let errors = [];

			// Loop through errors
			data.errors.forEach(i => {
				const el = this.el.querySelector("[name='" + i + "']");
				el.classList.add("error");
				el.closest("label").classList.add("error");

				errors.push(el);
			});

			// If errors, scroll to first error
			if(errors.length > 0) {
				this.showMessage(errors[0]);
			}
		}

		this.stopLoader();
	}

	error() {
		this.el.querySelector("[data-fetch-error]").style.display = "block";
		this.stopLoader();
	}

	showMessage(el, show) {
		if(show) {
			el.style.display = "block";
		}

		// Scroll offset
		let offset = 20;

		if(window.matchMedia("(min-width: " + d.breakpoints.medium + ")").matches) {
			offset = 20;
		}

		// Scroll only if target not on screen
		let top = el.getBoundingClientRect().top;

		if(top < offset) {
			let pos = el.getBoundingClientRect().top + d.scrollTop() - offset;
			
			window.scroll({top: pos, left: 0, behavior: "smooth"});	
		}
	}

	startLoader() {
		this.sending = true;
		this.button.querySelector(".c-loader").classList.add("m-active");
	}

	stopLoader() {
		this.sending = false;
		this.button.querySelector(".c-loader").classList.remove("m-active");
	}
}