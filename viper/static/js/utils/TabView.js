class TabView extends Utils {

	constructor(el) {
		super(el);
		
		this.build();
	}

	static setup(arr) {
		return super.setup(arr, TabView, "[data-tabview]");
	}

	build() {
		$$(".tab-nav li", this.el).forEach(i => {
			i.addEventListener("click", (e) => {
				this.onClickNav(i);
			});
		});
	}

	onClickNav(el) {
		// Clear all
		$$(".tab-nav li", this.el).forEach(i => {
			i.classList.remove("m-active");
		});

		$$(".tab-content", this.el).forEach(i => {
			i.classList.remove("m-active");
		});

		// Select current nav item
		el.classList.add("m-active");

		// Select current content
		const id = el.getAttribute("data-id");

		const content = this.el.querySelector(".tab-content[data-id='" + id + "']");
		content.classList.add("m-active");

		// Dispatch event
		this.dispatchEvent("open", {detail: {id: id}});
	}
}