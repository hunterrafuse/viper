class Filter extends Utils {

	constructor(el, target) {
		super(el);

		this.target = d("[data-filter-target='" + el.getAttribute("data-filter") + "']");

		this.build();
	}

	static setup(arr) {
		return super.setup(arr, Filter, "[data-filter]");
	}

	build() {
		$$("button", this.el).forEach(i => {
			i.addEventListener("click", e => {
				$$("button", this.el).forEach(j => {
					j.classList.remove("m-active");
				});

				i.classList.toggle("m-active");
				this.update(i.getAttribute("data-id"));
			});
		});
	}


	update(id) {
		// Hide all
		$$(".category", this.target).forEach(i => {
			i.classList.add("m-hidden");
		});

		// Show category
		if(id == "all") {
			$$(".category", this.target).forEach(i => {
				i.classList.remove("m-hidden");
			});
		}
		else {
			$$("[data-id='" + id + "']", this.target).forEach(i => {
				i.classList.remove("m-hidden");
			});
		}
	}
}