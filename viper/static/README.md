##Style Directory

If you decide to edit the styles and add new sheets, correctively add them to this directory and keep in mind the format of the file. Kind of similar to the example below:


`_example.scss`


so the sass compiler can correctly transform sass files into css files for correct styling. Then if you do make that file please add it to the \_variables.scss file like this:


`@import 'example'`


then you should be ready to go. If you have any issues it's probably you and not the software. Report a bug, do not touch any python if it is possibly not working. Make sure you check the command as for now you have to have it target a specific path to both the sass and css or it will not work.

Enjoy.

p.s. will change the command to a more easier command like `sass --watch scss` or something similar as the original command is kind of long.
