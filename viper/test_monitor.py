#!/usr/bin/env python

import tornado.ioloop
import tornado.web
import pprint
import logging

class MyDumpHandler(tornado.web.RequestHandler):
    def post(self):
        
        while True:
            pprint.pprint(self.request)
            pprint.pprint(self.request.body)
            l = open('viper/logs/app.log', 'w')
            return l

if __name__ == "__main__":
    tornado.web.Application([(r"/.*", MyDumpHandler),]).listen(8881)
    tornado.ioloop.IOLoop.instance().start()