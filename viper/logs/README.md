## Logs

All logs are in `.json` format
if you need to change this don't as the system
that will be set up in the future will require
to run on json files. If you change this to
for example CSV please converse with me before doing so.
Thanks.
