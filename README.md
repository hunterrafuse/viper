## Summary

Viper is a Python cross-platform tool for monitoring resources of OS: CPU, memory, disk, network. It uses the tornado framework but might be moved to a laravel mix of laravel and python.

## Requirements

- OS: Linux, Windows, FreeBSD, Solaris
- Python version: 3.X & newer
- Packages:
  - psutil
  - netifaces
  - wmi (only on Windows)
  - enum34 (only on python 3.0.0 - 3.4.0)
  - pip3 and newer
  - tornado
  - sass python package

## How to install

Run normally, not as root user:

- `cd viper/`

- `python3 setup.py install`

- `cd viper/`

- `python3 start.py`

Port runs on localhost:8888

## Editing styles?

If you want to edit some styles there is a simple sass compiler script added. Just target the static directory and it should be fine.

```
cd viper/
```

then:

```
$ sass --watch ./viper/static/scss/:./viper/static/css/
```

The output should be as follows:

```
$ sass --watch ./static/sass/:./static/css/
>>> Sass is watching for changes. Press Ctrl-C to stop.
      write ./static/css/variables.css
      write ./static/css/variables.css.map
```

##If you decide to edit

If you decide to edit this project please understand you need to create your own branch and checkout to
said branch so as to not create conflict. Then make a merge request and I will review it. Please do NOT
push to master or your access will be revoked. Thanks.
