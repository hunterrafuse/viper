psutil>=2.1.1
netifaces>=0.10.4
pyvalid>=0.9
pytest>=2.6.2
enum34>=1.0
six>=1.0
sass>=3.5.7
git+git://github.com/jberentsson/pytemp@master